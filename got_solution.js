// - 1. Write a function called `countAllPeople` which counts the total number of people in `got` variable defined in `data.js` file.

const got = require("../data-1");

function countAllPeople(got) {
  let result = 0;
  return objectTracker(got);

  function objectTracker(got) {
    for (let key in got) {
      if (String(key) == "people") {
        result += got[key].length;
      } else if (Array.isArray(got[key])) {
        arrayTracer(got[key]);
      } else if (typeof got[key] == "object") {
        objectTracker(got[key]);
      }
    }
    return result;
  }

  function arrayTracer(arr) {
    for (let i = 0; i < arr.length; i++) {
      if (typeof arr[i] == "object") {
        objectTracker(arr[i]);
      } else {
      }
    }
  }
}

// console.log(countAllPeople(got));

// - 2. Write a function called `peopleByHouses` which counts the total number of people in different houses in the `got` variable defined in `data.js` file.

function peopleByHouses(got) {
  let result = [];
  return objectTracker(got);

  function objectTracker(got) {
    for (let key in got) {
      if (String(key) == "people") {
        result.push([got.name, got[key].length]);
      } else if (Array.isArray(got[key])) {
        arrayTracer(got[key]);
      } else if (typeof got[key] == "object") {
        objectTracker(got[key]);
      }
    }
    return result;
  }

  function arrayTracer(arr) {
    for (let i = 0; i < arr.length; i++) {
      if (typeof arr[i] == "object") {
        objectTracker(arr[i]);
      } else {
      }
    }
  }
}

// console.log(peopleByHouses(got))

// 3. Write a function called `everyone` which returns a array of names of all the people in `got` variable.

function everyone(got) {
  let result = [];
  return objectTracker(got);

  function objectTracker(got) {
    for (let key in got) {
      if (String(key) == "people") {
        for (let innerKey in got[key]) {
          result.push(got[key][innerKey].name);
        }
      } else if (Array.isArray(got[key])) {
        arrayTracer(got[key]);
      } else if (typeof got[key] == "object") {
        objectTracker(got[key]);
      }
    }
    return result;
  }

  function arrayTracer(arr) {
    for (let i = 0; i < arr.length; i++) {
      if (typeof arr[i] == "object") {
        objectTracker(arr[i]);
      } else {
      }
    }
  }
}

// console.log(everyone(got))

// Write a function called `nameWithS` which returns a array of names of all the people in `got` variable whose name includes `s` or `S`.

function nameWithS(got) {
  let result = [];
  return objectTracker(got);

  function objectTracker(got) {
    for (let key in got) {
      if (String(key) == "people") {
        for (let innerKey in got[key]) {
          if (
            got[key][innerKey].name.includes("s") ||
            got[key][innerKey].name.includes("S")
          ) {
            result.push(got[key][innerKey].name);
          }
        }
      } else if (Array.isArray(got[key])) {
        arrayTracer(got[key]);
      } else if (typeof got[key] == "object") {
        objectTracker(got[key]);
      }
    }
    return result;
  }

  function arrayTracer(arr) {
    for (let i = 0; i < arr.length; i++) {
      if (typeof arr[i] == "object") {
        objectTracker(arr[i]);
      } else {
      }
    }
  }
}

// console.log(nameWithS(got));

// - 5. Write a function called `nameWithA` which returns a array of names of all the people in `got` variable whose name includes `a` or `A`.

function nameWithA(got) {
  let result = [];
  return objectTracker(got);

  function objectTracker(got) {
    for (let key in got) {
      if (String(key) == "people") {
        for (let innerKey in got[key]) {
          if (
            got[key][innerKey].name.startsWith("A") ||
            got[key][innerKey].name.startsWith("a")
          ) {
            result.push(got[key][innerKey].name);
          }
        }
      } else if (Array.isArray(got[key])) {
        arrayTracer(got[key]);
      } else if (typeof got[key] == "object") {
        objectTracker(got[key]);
      }
    }
    return result;
  }

  function arrayTracer(arr) {
    for (let i = 0; i < arr.length; i++) {
      if (typeof arr[i] == "object") {
        objectTracker(arr[i]);
      } else {
      }
    }
  }
}

// console.log(nameWithA(got));

// - 6. Write a function called `surnameWithS` which returns a array of names of all the people in `got` variable whoes surname is starting with `S`(capital s).

function surNameWiths(got) {
  let result = [];
  return objectTracker(got);

  function objectTracker(got) {
    for (let key in got) {
      if (String(key) == "people") {
        for (let innerKey in got[key]) {
          const split = got[key][innerKey].name.split(" ");
          if (split[1].startsWith("S") || split[1].startsWith("s")) {
            result.push(got[key][innerKey].name);
          }
        }
      } else if (Array.isArray(got[key])) {
        arrayTracer(got[key]);
      } else if (typeof got[key] == "object") {
        objectTracker(got[key]);
      }
    }
    return result;
  }

  function arrayTracer(arr) {
    for (let i = 0; i < arr.length; i++) {
      if (typeof arr[i] == "object") {
        objectTracker(arr[i]);
      } else {
      }
    }
  }
}

// console.log(surNameWiths(got))

// - 7. Write a function called `surnameWithA` which returns a array of names of all the people in `got` variable whoes surname is starting with `A`(capital a).

function surNameWithA(got) {
  let result = [];
  return objectTracker(got);

  function objectTracker(got) {
    for (let key in got) {
      if (String(key) == "people") {
        for (let innerKey in got[key]) {
          const split = got[key][innerKey].name.split(" ");
          if (split[1].startsWith("A")) {
            result.push(got[key][innerKey].name);
          }
        }
      } else if (Array.isArray(got[key])) {
        arrayTracer(got[key]);
      } else if (typeof got[key] == "object") {
        objectTracker(got[key]);
      }
    }
    return result;
  }

  function arrayTracer(arr) {
    for (let i = 0; i < arr.length; i++) {
      if (typeof arr[i] == "object") {
        objectTracker(arr[i]);
      } else {
      }
    }
  }
}

// console.log(surNameWithA(got));

// - 8. Write a function called `peopleNameOfAllHouses` which returns an object with the key of the name of house and value will be all the people in the house in an array.

function peopleNameOfAllHouses(got) {
  let result = {};
  return objectTracker(got);

  function objectTracker(got) {
    for (let key in got) {
      if (String(key) == "people") {
        let peopleOfHouse = [];
        for (let innerKey in got[key]) {
          peopleOfHouse.push(got[key][innerKey].name);
        }
        result = { ...result, [got.name]: peopleOfHouse };
      } else if (Array.isArray(got[key])) {
        arrayTracer(got[key]);
      } else if (typeof got[key] == "object") {
        objectTracker(got[key]);
      }
    }
    return result;
  }

  function arrayTracer(arr) {
    for (let i = 0; i < arr.length; i++) {
      if (typeof arr[i] == "object") {
        objectTracker(arr[i]);
      } else {
      }
    }
  }
}

console.log(peopleNameOfAllHouses(got));
